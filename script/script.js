/**
 * @author Castiel Le
 */

"use strict";
/**
 * @desc when the DOM is loaded, call the setup function
 */
document.addEventListener("DOMContentLoaded", setup);


/**
 * @desc create a btn variable and add an event listener to it. When the btn is clicked, call the fetchQuote function
 */
function setup(){
    let btn = document.querySelector('#btn');
    btn.addEventListener('click', fetchQuote);
}

/**
 * @desc create an url variable stores the api link and fetch. If there is no error(status code: 200), call the displayQuote function, else, call the errorFetch function
 */
function fetchQuote(){
    let url = "https://ron-swanson-quotes.herokuapp.com/v2/quotes";
    fetch(url)
    .then(response => {
        if (response.ok) {
            return response.json();
        
        }
    })
    .then(quote => displayQuote(quote))
    .catch(error => errorFetch(error));
}

/**
 * @desc display the quote that is fetched from the api url on the page
 * @param {*} quote content of the fetched url
 */
function displayQuote(quote){
    let quotepara = document.querySelector('#quote');
    quotepara.textContent = quote[0];
}

/**
 * @desc display a warning if there is an error when fetching the api url
 * @param {*} error the error code of the fetch url
 */
function errorFetch(error){
    let errorpara = document.querySelector('#error');
    errorpara.textContent = error + "! Please try again!"
}